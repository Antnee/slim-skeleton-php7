<?php
return [
    'settings' => [
        // Slim
        'determineRouteBeforeAppMiddleware' => false,
        'displayErrorDetails' => (bool)getenv('DISPLAY_ERRORS'),

        // Database
        'db' => [
            'dsn'  => 'mysql:dbname=' . getenv('DB_NAME') . ';host=' . getenv('DB_HOST'),
            'user' => getenv('DB_USER'),
            'pass' => getenv('DB_PASS'),
            'opts' => [
                PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION,
            ],
        ],

        // Logging
        'logger' => [
            'name'  => 'App',
            'path'  => __DIR__ . '/../log/'.(new DateTimeImmutable("now", new DateTimeZone("UTC")))->format("Ymd").".log",
            'level' => (int)getenv('LOG_LEVEL'), // See Monolog\Logger constants
        ],

        // Twig
        'view' => [
            'templateDir' => __DIR__ . '/Template/',
            'twig' => [
                'cache'       => __DIR__ . '/../cache/twig/',
                'debug'       => (bool)getenv('VIEW_DEBUG'),
                'auto_reload' => (bool)getenv('VIEW_AUTO_RELOAD'),
            ],
        ],
    ]
];